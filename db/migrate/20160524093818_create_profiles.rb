class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :last_name
      t.string :contact1
      t.string :contact2
      t.string :income
      t.belongs_to :users
      t.timestamps
    end
  end
end
