class CreateProfileImages < ActiveRecord::Migration
  def change
    create_table :profile_images do |t|
      t.string :image
      t.belongs_to :user
      t.timestamps
    end
  end
end
