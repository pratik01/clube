class CreateFamailyinfos < ActiveRecord::Migration
  def change
    create_table :famailyinfos do |t|
      t.string :father_name
      t.string :monther
      t.string :father_occupation
      t.string :mother_occupation
      t.string :income
      t.string :no_of_sister
      t.string :no_of_brother
      t.string :contact1
      t.string :contact2
      t.belongs_to :users
      t.timestamps
    end
  end
end
