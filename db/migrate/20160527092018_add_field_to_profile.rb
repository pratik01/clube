class AddFieldToProfile < ActiveRecord::Migration
  def change
    add_column :profiles ,:date_of_birth,:date
    add_column :profiles, :gender ,:string
    add_column :profiles, :education,:string
  end
end
