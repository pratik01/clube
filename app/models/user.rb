class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :profile
  has_one :famailyinfo
  has_many :addresses
  has_many :profile_images
  accepts_nested_attributes_for :profile
  accepts_nested_attributes_for :famailyinfo
  accepts_nested_attributes_for :addresses
  accepts_nested_attributes_for :profile_images
end
