class RegistrationsController < Devise::RegistrationsController

  def edit
    @user = User.find(current_user.id)
    if @user.profile.nil? || @user.profile.blank?
      @user.profile.build
    end
    if @user.famailyinfo.nil? || @user.famailyinfo.blank?
      @user.famailyinfo.build
    end
    if @user.addresses.nil? || @user.addresses.blank?
      1.times { @user.addresses.build }
    end
    if @user.profile_images.nil? || @user.profile_images.blank?
      5.times { @user.profile_images.build }
    end
  end

  def update
    @user = User.find(current_user.id)
    if @user.update(user_params)
      redirect_to "/"
    else
      render :action => 'edit'
    end
  end

  private
  def user_params
    params.require(:user).permit(:email, :password,
    profile_attributes: [:id,:first_name, :last_name,:contact1,:contact2,:date_of_birth,:gender,:education],
    profile_images_attributes: [:id,:image],
    famailyinfo_attributes: [:id,:father_name,:monther,:father_occupation,:mother_occupation,:income,:no_of_sister,:no_of_brother,:contact1,:contact2],
    addresses_attributes: [:id,:address1,:address2,:city,:state,:country,:zip_code])
  end

end
